class CreateClientMessages < ActiveRecord::Migration
  def change
    create_table :client_messages do |t|
      t.string :name
      t.string :telephone
      t.string :email
      t.text :message

      t.timestamps null: false
    end
  end
end
