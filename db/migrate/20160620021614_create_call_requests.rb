class CreateCallRequests < ActiveRecord::Migration
  def change
    create_table :call_requests do |t|
      t.string :name
      t.string :telephone

      t.timestamps null: false
    end
  end
end
