Rails.application.routes.draw do

	# apipie
	apipie

	# devise
	devise_for :users, controllers: {
		sessions: 'user/sessions',
		registrations: 'user/registrations',
		passwords: 'user/passwords'
	}

	# site
	namespace :site, :path => '' do
		root to: 'main#index'
		# get '/' => 'main#index'


		post '/call_request', to: 'call_requests#call_request'
		post '/send_message', to: 'client_messages#send_message'
	end

	# admin
	namespace :admin do
		root to: 'admin#base'
		get 'dashboard', to: 'dashboard#index'
		resources :users
		resources :user_roles
		resources :call_requests
		resources :client_messages

		# ajax
		patch 'file/load' => 'entity_file#load', :as => 'file_load'
		post 'file/remove' => 'entity_file#remove', :as => 'file_remove'
	end

	# api
	namespace :api, defaults: {format: 'json'} do
		namespace :v1 do
			# user
			get 'users' => 'users#list'
			post 'users' => 'users#create'
			get 'login' => 'users#login'
			get 'logout' => 'users#logout'

			# notice
			get 'notice/allow' => 'users#allow_notice'
			get 'notice/disallow' => 'users#disallow_notice'

			# social
			get 'login/fb' => 'users#fb_login'
			get 'login/vk' => 'users#vk_login'
		end
	end
end
