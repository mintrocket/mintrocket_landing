class FormsMailer < ApplicationMailer
	def call_request(item, email)

		@item = item

		mail(to: email, subject: 'Запрос на звонок 5weeks.ru')

	end

	def client_message(item, email)

		@item = item

		mail(to: email, subject: 'Сообщение от пользователя 5weeks.ru')
	end
end
