class ApplicationMailer < ActionMailer::Base
  default from: 'hello@mintmail.ru'
  layout 'mailer/mailer'
end
