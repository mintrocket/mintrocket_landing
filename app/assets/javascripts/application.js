// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//

// = require site/jquery-1.11.1.min

// = require site/mask/jquery.inputmask
// = require site/mask/jquery.inputmask.extensions.js
// = require site/mask/jquery.inputmask.date.extensions
// = require site/mask/jquery.inputmask.numeric.extensions
// = require site/mask/jquery.inputmask.custom.extensions
// = require fotorama
// = require site/carousels/slick/slick
// = require site/magnific/jquery.magnific-popup
// = require site/app


