//var ready;
//ready = function() {
//
//	// slick carousel
//	var carousel = $('.slick-carousel');
//	carousel.each(function(){
//		var _this = $(this);
//		_this.slick({
//			infinite: true,
//			slidesToShow: 6,
//			slidesToScroll: 1,
//			initialSlide: 0
//		});
//
//		var prev = _this.find('.slick-prev');
//		var next = _this.find('.slick-next');
//		prev.html('<i class="icon-carousel-prev"></i>');
//		next.html('<i class="icon-carousel-next"></i>');
//	});
//
//	// scroll to top
//	$('body').on('click', 'a.nav-scroll', function(event){
//		$('html,body').scrollTo(this.hash, this.hash, {offset: 0});
//	});
//
//	// pickdate
//	var pickdate_default = {
//		'monthsFull': ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь' ],
//		'monthsShort': [ 'Янв', 'Февр', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сент', 'Окт', 'Нояб', 'Дек' ],
//		'weekdaysFull': [ 'Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота' ],
//		'weekdaysShort': [ 'Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб' ],
//		'format': 'dd.mm.yyyy',
//		'formatSubmit': 'yyyy/mm/dd',
//		'hiddenPrefix': 'prefix__',
//		'hiddenSuffix': '__suffix',
//		'today': 'Сегодня',
//		'clear': 'Очистить',
//		'close': 'Закрыть',
//		'firstDay': 1,
//		'selectMonths': true,
//		'selectYears': 100,
//		'max': true
//	};
//
//	var $datepicker = $('.datepicker').pickadate(pickdate_default);
//	var datepicker = $datepicker.pickadate('picker');
//
//};
//
//$(document).ready(ready);
//$(document).on('page:load', ready);


$(document).ready(function(){

    $('.partners_slider').slick({
        infinite: false,
        slidesToShow: 4,
        slidesToScroll: 1
    });

    $('.cases_slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true

    });


    var telephone = $('.telephone');

    if(telephone.length > 0){
        $('.telephone').inputmask('+7 (999) 999-99-99')
    }



    var menu = $('.header_menu');

    if(menu.length > 0){
        menu.on('click', 'a', function(event){
            event.preventDefault();
            var href_id = $(this).attr('href'), top = $(href_id).offset().top;



            $('body,html').animate({scrollTop: top}, 1000);
        });
    }

    var arrow_down = $('.part_1_arrow_down');

    if(arrow_down.length > 0){
        arrow_down.on('click', function(event){
            event.preventDefault();
            var href_id = $(this).attr('href'), top = $(href_id).offset().top;



            $('body,html').animate({scrollTop: top}, 1000);
        });
    }


	var call_modal = $('.call_me__bottom a');

	if(call_modal.length > 0) {
		call_modal.on('click', function (e) {
			e.preventDefault();

			$.magnificPopup.open({
				removalDelay: 500,
				items: {
					src: '#modal_call'
				},
				overflowY: 'hidden',
				callbacks: {
					beforeOpen: function (e) {
						this.st.mainClass = call_modal.attr('data-effect');

					},
					close: function (e) {
						this.st.mainClass = call_modal.attr('data-effect');

						//input.val('');
						//name.val('');
						//telephone.val('');
						//email.val('');

					}
				},
				midClick: true
			});
		});
	}

	var message_send = $('.get_consult');

	message_send.on('click', function(e){
		e.preventDefault();

		$.magnificPopup.open({
			removalDelay: 500,
			items: {
				src: '#modal_consult'
			},
			overflowY: 'hidden',
			callbacks: {
				beforeOpen: function (e) {
					this.st.mainClass = call_modal.attr('data-effect');

				},
				close: function (e) {
					this.st.mainClass = call_modal.attr('data-effect');

				}
			},
			midClick: true
		});
	});


	var cost_button = $('.cost_button');

	cost_button.on('click', function(e){
		e.preventDefault();

		$.magnificPopup.open({
			removalDelay: 500,
			items: {
				src: '#modal_cost'
			},
			overflowY: 'hidden',
			callbacks: {
				beforeOpen: function (e) {
					this.st.mainClass = call_modal.attr('data-effect');

				},
				close: function (e) {
					this.st.mainClass = call_modal.attr('data-effect');

				}
			},
			midClick: true
		});
	});

	var modal_submit = $('.modal_call_button');

	modal_submit.on('click', function(e){
		e.preventDefault();

		var name = $(this).parent().find($('.name'));
		var telephone = $(this).parent().find($('.telephone'));

		if(name.val() != '' && telephone.val() != ''){
			setTimeout(function(){
				$.magnificPopup.open({
					removalDelay: 500,
					items: {
						src: '#modal_complete'
					},
					overflowY: 'hidden'
				});
			}, 100);


			setTimeout(function(){
				$.magnificPopup.close({
					removalDelay: 500,
					items: {
						src: '#modal_complete'
					}
				});
			}, 3500);


			$.ajax({
				url: '/call_request.js',
				type: 'POST',
				dataType: 'json',
				data: {
					name: name.val(),
					telephone: telephone.val()

				}
			}).done(function(result) {

				$('#modal_complete .modal_title').html('ЗАКАЗАТЬ ЗВОНОК');

			}).fail(function() {
				console.log("error");
			});



		}
		else{

			if(name.val() == "")
				name.addClass('valid_error');

			if(telephone.val() == "")
				telephone.addClass('valid_error');

			setTimeout(function(){
				name.removeClass('valid_error');
				telephone.removeClass('valid_error');
			}, 2000);
		}




	});




	var send_message = $('.modal_message_button');

	send_message.on('click', function(e){
		e.preventDefault();

		var name = $(this).parent().find($('.name'));
		var telephone = $(this).parent().find($('.telephone'));
		var email = $(this).parent().find($('.email'));
		var message = $(this).parent().find($('textarea'));

		if(name.val() != '' && telephone.val() != '' && email.val() != '' && message.val() != ''){


			var title = send_message.parent().find($('.modal_title')).html();




			$.ajax({
				url: '/send_message.js',
				type: 'POST',
				dataType: 'json',
				data: {
					name: name.val(),
					telephone: telephone.val(),
					message: message.val().replace(/(?:\r\n|\r|\n)/g, '<br />'),
					email: email.val()
				}
			}).done(function(result) {

				$.magnificPopup.open({
					removalDelay: 500,
					items: {
						src: '#modal_complete'
					},
					overflowY: 'hidden'
				});

				setTimeout(function(){
					$.magnificPopup.close({
						removalDelay: 500,
						items: {
							src: '#modal_complete'
						}
					});
				}, 3500);

				$('#modal_complete .modal_title').html(title);


			}).fail(function() {
				console.log("error");
			});



		}
		else{

			if(name.val() == "")
				name.addClass('valid_error');

			if(telephone.val() == "")
				telephone.addClass('valid_error');

			if(email.val() == "")
				email.addClass('valid_error');

			if(message.val() == "")
				message.addClass('valid_error');

			setTimeout(function(){
				name.removeClass('valid_error');
				telephone.removeClass('valid_error');
				email.removeClass('valid_error');
				message.removeClass('valid_error');
			}, 2000);
		}




	});





	var cost_button = $('.ajax_cost');

	cost_button.on('click', function(e){
		e.preventDefault();

		var name = $(this).parent().find($('.name'));
		var telephone = $(this).parent().find($('.telephone'));

		if(name.val() != '' && telephone.val() != ''){


			$('.call_us_form').css("display", "none");
			$('.call_us_desc').html('Спасибо, ваша заявка принята. Мы свяжемся с вами в ближайшее время.');


			$.ajax({
				url: '/call_request.js',
				type: 'POST',
				dataType: 'json',
				data: {
					name: name.val(),
					telephone: telephone.val()
				}
			}).done(function(result) {






			}).fail(function() {
				console.log("error");
			});



		}
		else{

			if(name.val() == "")
				name.addClass('valid_error');

			if(telephone.val() == "")
				telephone.addClass('valid_error');

			if(email.val() == "")
				email.addClass('valid_error');

			if(message.val() == "")
				message.addClass('valid_error');

			setTimeout(function(){
				name.removeClass('valid_error');
				telephone.removeClass('valid_error');
				email.removeClass('valid_error');
				message.removeClass('valid_error');
			}, 2000);
		}




	});

	var presentation = $('.presentation_request');

	presentation.on('click', function(e){
		e.preventDefault();

		var name = $(this).parent().find($('.name'));
		var telephone = $(this).parent().find($('.telephone'));

		if(name.val() != '' && telephone.val() != ''){


			$(this).parent().find('.name').css('display', 'none');
			$(this).parent().find('.telephone').css('display', 'none');
			$(this).css('display', 'none');

			$(this).parent().find('.form_desc').html('Спасибо, ваша заявка принята. Мы свяжемся с вами в ближайшее время.');


			$.ajax({
				url: '/call_request.js',
				type: 'POST',
				dataType: 'json',
				data: {
					name: name.val(),
					telephone: telephone.val()
				}
			}).done(function(result) {






			}).fail(function() {
				console.log("error");
			});



		}
		else{

			if(name.val() == "")
				name.addClass('valid_error');

			if(telephone.val() == "")
				telephone.addClass('valid_error');

			if(email.val() == "")
				email.addClass('valid_error');

			if(message.val() == "")
				message.addClass('valid_error');

			setTimeout(function(){
				name.removeClass('valid_error');
				telephone.removeClass('valid_error');
				email.removeClass('valid_error');
				message.removeClass('valid_error');
			}, 2000);
		}




	});




	var footer_request = $('.footer_request');

	footer_request.on('click', function(e){
		e.preventDefault();

		var name = $(this).parent().parent().find($('.name'));
		var telephone = $(this).parent().parent().find($('.telephone'));
		var email = $(this).parent().parent().find($('.email'));
		var message = $(this).parent().parent().find($('textarea'));

		if(name.val() != '' && telephone.val() != '' && email.val() != '' && message.val() != ''){

			$('.questions_form').css('display', 'none');

			$('.questions_desc').html('Спасибо, ваша заявка принята. Мы свяжемся с вами в ближайшее время.');

			$.ajax({
				url: '/send_message.js',
				type: 'POST',
				dataType: 'json',
				data: {
					name: name.val(),
					telephone: telephone.val(),
					message: message.val().replace(/(?:\r\n|\r|\n)/g, '<br />'),
					email: email.val()
				}
			}).done(function(result) {



			}).fail(function() {
				console.log("error");
			});



		}
		else{

			if(name.val() == "")
				name.addClass('valid_error');

			if(telephone.val() == "")
				telephone.addClass('valid_error');

			if(email.val() == "")
				email.addClass('valid_error');

			if(message.val() == "")
				message.addClass('valid_error');

			setTimeout(function(){
				name.removeClass('valid_error');
				telephone.removeClass('valid_error');
				email.removeClass('valid_error');
				message.removeClass('valid_error');
			}, 2000);
		}




	});

});