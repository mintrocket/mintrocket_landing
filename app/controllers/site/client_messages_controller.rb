module Site
	class ClientMessagesController < SiteController
		skip_before_action :verify_authenticity_token
		def send_message

			name = params[:name]
			phone = params[:telephone]
			email = params[:email]
			message = params[:message]

			@request = ClientMessage.create(  :name => name,
											:telephone => phone,
											:email => email,
											:message => message)




			unless name.nil? && phone.nil? && email.nil? && message.nil?

				respond = true

			else
				respond = false
			end


			if respond

				@send = FormsMailer.client_message(@request, 'roman@mintmail.ru').deliver
				@send = FormsMailer.client_message(@request, 'hello@mintmail.ru').deliver

				@send = FormsMailer.client_message(@request, 'vladimir@mintmail.ru').deliver


				respond_to do |format|
					format.js {render :json => {send: @request}}
				end
			else
				redirect_to site_root_path
			end


		end
	end
end