module Site
	class CallRequestsController < SiteController
		skip_before_action :verify_authenticity_token
	    def call_request

		    name = params[:name]
		    phone = params[:telephone]


		    @call_request = CallRequest.create(  :name => name,
		                                          :telephone => phone)




		    unless name.nil? && phone.nil?

			    respond = true

		    else
			    respond = false
		    end


		    if respond

			    @send = FormsMailer.call_request(@call_request, 'roman@mintmail.ru').deliver
			    @send = FormsMailer.call_request(@call_request, 'hello@mintmail.ru').deliver

			    @send = FormsMailer.call_request(@call_request, 'vladimir@mintmail.ru').deliver


			    respond_to do |format|
				    format.js {render :json => {send: @call_request}}
			    end
		    else
			    redirect_to site_root_path
		    end


	    end
	end
end