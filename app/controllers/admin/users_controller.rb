module Admin
	class UsersController < AdminController
		include Admin::EntityFiles

		before_action :set_user, only: [:edit, :update, :destroy]

		# gem cancan
		authorize_resource :class => :admin_users

		# ====================
		#        INDEX
		# ====================
		def index
			@users = User.all
		end

		# ====================
		#        NEW
		# ====================
		def new
			@user = User.new
		end

		# ====================
		#        EDIT
		# ====================
		def edit
		end

		# ====================
		#       CREATE
		# ====================
		def create
			@user = User.new(user_params_create)

			# save
			if @user.save
				respond = true
			else
				respond = false
			end

			# redirect
			redirect = admin_users_path

			respond_to do |format|
				if respond
					format.html { redirect_to redirect, notice: "Пользователь <span class=\"fwb\">#{@user.email}</span> успешно создан"}
					format.json { head :no_content }
				else
					format.html { render action: 'new' }
					format.json { head :no_content }
				end
			end
		end

		# ====================
		#       UPDATE
		# ====================
		def update

			# update
			if @user.update(user_params_update)
				respond = true
			else
				respond = false
			end

			# redirect
			redirect = admin_users_path

			respond_to do |format|
				if respond
					format.html { redirect_to redirect, notice: "Пользователь <span class=\"fwb\">#{@user.email}</span> успешно обновлен" }
					format.json { head :no_content }
				else
					format.html { render action: 'edit' }
					format.json { render json: @user.errors, status: :unprocessable_entity }
				end
			end
		end

		# ====================
		#       DELETE
		# ====================
		def destroy
			@user.destroy

			# redirect
			redirect = admin_users_path

			respond_to do |format|
				format.html { redirect_to redirect }
				format.json { head :no_content }
			end
		end

		# ====================
		#       PRIVATE
		# ====================
		private
			def set_user
				@user = User.find(params[:id])
			end

			def user_params_update
				if (!params[:user][:password].nil?) && (params[:user][:password].length > 0)
					params[:user].permit(:email, :first_name, :second_name, :last_name, :user_role_id,
					                     :user_status_id, :password)
				else
					params[:user].permit(:email, :first_name, :second_name, :last_name, :user_role_id,
					                     :user_status_id, :personal_data)
				end
			end

			def user_params_create
				params[:user].permit(:email, :first_name, :second_name, :last_name, :user_role_id,
				                     :user_status_id, :password)
			end
	end
end