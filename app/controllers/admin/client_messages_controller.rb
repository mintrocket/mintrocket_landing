module Admin
	class ClientMessagesController < AdminController
		before_action :set_client_message, only: [:edit, :update, :destroy]

		# gem cancan
		load_and_authorize_resource
		authorize_resource :class => :admin_client_messages

		# ====================
		#        INDEX
		# ====================
		def index
			@client_messages = ClientMessage.all
		end

		# ====================
		#        NEW
		# ====================
		def new
			@client_message = ClientMessage.new
		end

		# ====================
		#        EDIT
		# ====================
		def edit
		end

		# ====================
		#       CREATE
		# ====================
		def create
			@client_message = ClientMessage.new(client_message_params)

			# save
			if @client_message.save
				respond = true
			else
				respond = false
			end

			# redirect
			redirect = admin_client_messages_path

			respond_to do |format|
				if respond
					format.html { redirect_to redirect, notice: 'Запись успешно создана'}
					format.json { head :no_content }
				else
					format.html { render action: 'new' }
					format.json { head :no_content }
				end
			end
		end

		# ====================
		#       UPDATE
		# ====================
		def update

			# update
			if @client_message.update(client_message_params)
				respond = true
			else
				respond = false
			end

			# redirect
			redirect = admin_client_messages_path

			respond_to do |format|
				if respond
					format.html { redirect_to redirect, notice: 'Запись успешно обновлена' }
					format.json { head :no_content }
				else
					format.html { render action: 'edit' }
					format.json { render json: @client_message.errors, status: :unprocessable_entity }
				end
			end
		end

		# ====================
		#       DELETE
		# ====================
		def destroy
			@client_message.destroy

			# redirect
			redirect = admin_client_messages_path

			respond_to do |format|
				format.html { redirect_to redirect }
				format.json { head :no_content }
			end
		end

		# ====================
		#       PRIVATE
		# ====================
		private

		def client_message_params
			params[:client_message].permit(:name, :telephone, :email, :message)
		end

	end
end