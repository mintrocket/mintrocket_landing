module Admin
    class AdminController < ApplicationController

	    before_action :authenticate_user!

	    layout 'admin/application'

		def admin_controller?
			true
		end


	    def base
		    redirect_to admin_dashboard_path

		    ## redirects if need
		    # if current_user.role? :tourist
		    # 	redirect_to admin_applications_path
		    # elsif current_user.role? :member_mkk
		    # 	redirect_to edit_admin_user_path(current_user)
		    # elsif current_user.role? :specialist
		    # 	redirect_to edit_admin_user_path(current_user)
		    # elsif current_user.role? :admin
		    # 	redirect_to admin_posters_path
		    # end
	    end

	    rescue_from CanCan::AccessDenied do |exception|
		    redirect_to admin_root_url, :notice => 'У вас недостаточно прав, чтобы просмотреть данный раздел' #exception.message
	    end
    end
end
