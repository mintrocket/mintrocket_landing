module Admin
	class CallRequestsController < AdminController
		before_action :set_call_request, only: [:edit, :update, :destroy]

		# gem cancan
		load_and_authorize_resource
		authorize_resource :class => :admin_call_requests

		# ====================
		#        INDEX
		# ====================
		def index
			@call_requests = CallRequest.all
		end

		# ====================
		#        NEW
		# ====================
		def new
			@call_request = CallRequest.new
		end

		# ====================
		#        EDIT
		# ====================
		def edit
		end

		# ====================
		#       CREATE
		# ====================
		def create
			@call_request = CallRequest.new(call_request_params)

			# save
			if @call_request.save
				respond = true
			else
				respond = false
			end

			# redirect
			redirect = admin_call_requests_path

			respond_to do |format|
				if respond
					format.html { redirect_to redirect, notice: 'Запись успешно создана'}
					format.json { head :no_content }
				else
					format.html { render action: 'new' }
					format.json { head :no_content }
				end
			end
		end

		# ====================
		#       UPDATE
		# ====================
		def update

			# update
			if @call_request.update(call_request_params)
				respond = true
			else
				respond = false
			end

			# redirect
			redirect = admin_call_requests_path

			respond_to do |format|
				if respond
					format.html { redirect_to redirect, notice: 'Запись успешно обновлена' }
					format.json { head :no_content }
				else
					format.html { render action: 'edit' }
					format.json { render json: @call_request.errors, status: :unprocessable_entity }
				end
			end
		end

		# ====================
		#       DELETE
		# ====================
		def destroy
			@call_request.destroy

			# redirect
			redirect = admin_call_requests_path

			respond_to do |format|
				format.html { redirect_to redirect }
				format.json { head :no_content }
			end
		end

		# ====================
		#       PRIVATE
		# ====================
		private

		def call_request_params
			params[:call_request].permit(:name, :telephone)
		end

	end
end