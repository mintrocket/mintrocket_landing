class ClientMessage < ActiveRecord::Base

	include AdminBase

	# signatures
	self.signatures = {
			create_element: '',
			edit_element: '',
			add_element: '',
			elements: 'Сообщения',
			list_elements: 'Список сообщений'
	}

	# properties of table
	self.table_props = [{
			                    name: '#',
			                    machine: 'id',
			                    type: 'text',
			                    template: 'default',
			                    options: {
					                    icon: '',
					                    show_in_modal: true
			                    },
			                    roles: []
	                    }, {
			                    name: 'Имя',
			                    machine: 'name',
			                    type: 'text',
			                    template: 'default',
			                    options: {
					                    icon: ''
			                    },
			                    roles: []
	                    }, {
			                    name: 'Телефон',
			                    machine: 'telephone',
			                    type: 'text',
			                    template: 'default',
			                    options: {
					                    icon: ''
			                    },
			                    roles: []
	                    }, {
								name: 'E-Mail',
								machine: 'email',
								type: 'text',
								template: 'default',
								options: {
										icon: ''
								},
								roles: []
						}, {
								name: 'Сообщение',
								machine: 'message',
								type: 'text',
								template: 'default',
								options: {
										icon: ''
								},
								roles: []
						}]

	# field of form
	self.form_fields = []

	def table_row_color
	end

end
