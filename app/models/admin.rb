module Admin
	class Setting < ActiveRecord::Base
		class_attribute :menu

		self.menu = {admin: [],
		             user: []}

		self.menu[:admin] = [{
			                     name: 'Меню',
			                     link: 'label',
			                     options: {
				                     class: 'sidebar-label pt20'
			                     }
		                     },{
								name: 'Сообщения',
								link: 'admin_client_messages_path',
								options: {
										icon: 'icon-zoom2 icon-users2'
										# tray: 'Новое!'
								}
							}, {
								name: 'Запросы на звонок',
								link: 'admin_call_requests_path',
								options: {
										icon: 'icon-zoom2 icon-users2'
										# tray: 'Новое!'
								}
							},{
			                     name: 'Пользователи',
			                     link: 'admin_users_path',
			                     options: {
				                     icon: 'icon-zoom2 icon-users2'
				                     # tray: 'Новое!'
			                     }
		                     }, ]
	end
end